# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


from typing import Any, Text, Dict, List, Union
import requests
import re
#
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet
from rasa_sdk.forms import FormAction


class ActionAksJoeynlp(Action):

    def name(self) -> Text:
        return "action_ask_joeynlp"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        user_msg = tracker.latest_message['text']

        response = requests.post(
            'http://159.203.67.26:5000/translator/translate', json={'ask': user_msg})

        if response:
            # print(response)
            dicresponse = response.json()
            dispatcher.utter_message(dicresponse['Answer'])
        else:
            print('An error has occurred.')

        dispatcher.utter_message(user_msg)

        return []


class ActionFillSlots(Action):
    def name(self) -> Text:
        return "action_fill_slots"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # print(domain)
        # user_msg = tracker.latest_message

        print(tracker.sender_id)

        spl = tracker.sender_id.split("_")

        return [SlotSet("phone", spl[2]), SlotSet("name", spl[3]), SlotSet("age", spl[4]), SlotSet("city", spl[5])]


class ClageForm(FormAction):
    """Example of a custom form action"""

    def name(self):
        """Unique identifier of the form"""
        return "clage_form"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""

        return ["clage"]

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""
        return {
            "clage": self.from_entity(entity="clage")

        }

    def validate_clage(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Validate clage value."""

        print("Validate clage", value)
        # agenum = re.findall(r'\b\d+\b', value)
        agenum = re.findall(r'\d+',value)

        print("agenum",agenum)

        return {"clage": agenum[0]}

    def submit(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled"""

        # utter submit template
        # dispatcher.utter_template("utter_submit_clage", tracker)
        return []
