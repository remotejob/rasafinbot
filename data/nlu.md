## intent: greetings
- hei
- haloo
- heipä

## intent: masturbation
- runkkausta
- runkkaus
- masturbointi
- itsetyydytys
- masturbation

## intent: robot
- oot robotti
- oletko robotti
- oletko sinä robotti
- ootko oikea ihminen vai robotti
- ahaa olet robotti
- robot
- botin kanssa
- etkä botti
- botit
- botti
- botit säätänä
- botti kos se tääl

## intent:i_call_if
- kyllä soitan jos haluat tavataan ensin kun tulet asunulle
- kyllä ensin soitan ei ole muuta vaihtoehtoa jos haluan naida
- jos naidaan illalla ni soitan
- soitan haluan tavata sinua ja naida kiitos
- vastatko puhelun nyt jos soitan
- soitan jos saan naida ja silitää pyllyä ja halata olet minun suosikki
- soitan jos pääsee naimaan
- soitan jos saan pillua
- soitan jos naidaan
- soitan jos saan pitää kullia sinun pillussa
- soitan jos tulet asunolle ja annat pillua
- soitan jos kerrot minulle tuhmaa
- soitan jos keksimme kivaa kiitos
- soitan jos suostut
- joo minä soitan ja jos tavataan mitä tehdään
- soitan jos saa pillua heti

## intent:my_phone_number
- anna numero niin soitan
- mä soittaa sinulle
- soitan etsin sinut
- soitan huomenna
- kyllä soitan
- soitan illalla
- kyllä soitan puhelimessa on parempi sopia

## intent:how_it_going
- miten menee
- mitä kuuluu
- ihan hyvää

## intent:goodnight
- hyvää yötä
- anteeks hyvää yötä

## intent:call_me_first
- soita mulle
- soita sinä mulle
- soita sä mulle
- soitappa itse mulle
- soitatko mulle
- soitatko minulle
- jos oot kiinnostunut tapaamisesta soita sinä mulle


## intent:sorry
- anteeks
- anteeksi
- anteeksi kun viivyin
- sori
- sorry

## intent:dont_know
- miksi
- miksi ei
- kuinka
- minkä takia
- mistä syystä
- minkä tähden
- mistä johtuen
- mutta miksi
- miks et

## intent:location
- missä asut
- missä sinä asut
- missäpäin asut
- missä olet

## intent:meeting_offer
- tavata
- voidaanko tavata
- voidaanko joskus tavata
- tavataan jossain
- haluan tavata sinut
- tavataanko

## intent:thank_you
- kiitos
- kiitos ihana
- olen kiitollinen

## intent:goodbye
- hyvästi
- jäähyväiset
- bye
- goodbye
- see you around
- see you later
- so long

## intent:answer_name
- kuka olet
- kukas sielä

## intent:dont_call
- en soita
- en voi soitaa 060 numeroihin
- en voi 060 numerohin soitaa
- ei ole varaa soitaa
- en voi soittaa
- olkoo en voi soitaa
- en soita 0700
- en uskalla soittaa

## intent:expensive
- kallis numero
- kalliita numeroita
- se on tosi kalista
- ei riitä saldo sulla on maksullinen numero
- Kallista


## intent: ask_age
- kuinka vanha sinä olet
- kuin vanha oot
- kuinka vanha sä ot
- miten vanha olet
- kuinkas vanha oot
- kui vanha oot
- kuinka vanha sä olet
- kuin vanha nainen olet
- kuinka vanha sinä olet
- ok käunis tyttö kunka vanha sina olen
- ootko kuin vanha
- miten vanha olet
- minkä ikänen oot


## intent: answer_clage
- [11](clage)
- [12](clage)
- [13](clage)
- [14](clage)
- [15](clage)
- [16](clage)
- [17](clage)
- [18](clage)
- [19](clage)
- [20](clage)
- [21](clage)
- [22](clage)
- [23](clage)
- [24](clage)
- [25](clage)
- [26](clage)
- [27](clage)
- [28](clage)
- [29](clage)
- [30](clage)
- [31](clage)
- [32](clage)
- [33](clage)
- [34](clage)
- [35](clage)
- [36](clage)
- [37](clage)
- [38](clage)
- [39](clage)
- [40](clage)
- [41](clage)
- [42](clage)
- [43](clage)
- [44](clage)
- [45](clage)
- [46](clage)
- [47](clage)
- [48](clage)
- [49](clage)
- [50](clage)
- [51](clage)
- [52](clage)
- [53](clage)
- [54](clage)
- [55](clage)
- [56](clage)
- [57](clage)
- [58](clage)
- [59](clage)
- [60](clage)
- [61](clage)
- [62](clage)
- [63](clage)
- [64](clage)
- [65](clage)
- [66](clage)
- [67](clage)
- [68](clage)
- [69](clage)
- [70](clage)
- [71](clage)
- [72](clage)
- [73](clage)
- [74](clage)
- [75](clage)
- [11v](clage)
- [12v](clage)
- [13v](clage)
- [14v](clage)
- [15v](clage)
- [16v](clage)
- [17v](clage)
- [18v](clage)
- [19v](clage)
- [20v](clage)
- [21v](clage)
- [22v](clage)
- [23v](clage)
- [24v](clage)
- [25v](clage)
- [26v](clage)
- [27v](clage)
- [28v](clage)
- [29v](clage)
- [30v](clage)
- [31v](clage)
- [32v](clage)
- [33v](clage)
- [34v](clage)
- [35v](clage)
- [36v](clage)
- [37v](clage)
- [38v](clage)
- [39v](clage)
- [40v](clage)
- [41v](clage)
- [42v](clage)
- [43v](clage)
- [44v](clage)
- [45v](clage)
- [46v](clage)
- [47v](clage)
- [48v](clage)
- [49v](clage)
- [50v](clage)
- [51v](clage)
- [52v](clage)
- [53v](clage)
- [54v](clage)
- [55v](clage)
- [56v](clage)
- [57v](clage)
- [58v](clage)
- [59v](clage)
- [60v](clage)
- [61v](clage)
- [62v](clage)
- [63v](clage)
- [64v](clage)
- [65v](clage)
- [66v](clage)
- [67v](clage)
- [68v](clage)
- [69v](clage)
- [70v](clage)
- [71v](clage)
- [72v](clage)
- [73v](clage)
- [74v](clage)
- [75v](clage)